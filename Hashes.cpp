﻿// Hashes.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <list>

using namespace std;

class HashTable
{
    int Capacity;
    list<int>* Table;

public:
    HashTable(int V);
    void InsertItem(int key, int data);
    void DeleteItem(int key);

	/* Я не совсем понял зачем нужны эти две функции,
	разве они не должны использоватся в хеш-функции? */
    int CheckPrime(int n)
    {
        int i;
        if (n == 1 || n == 0)
        {
            return 0;
        }
        for (i = 2; i < n / 2; i++)
        {
            if (n % i == 0)
            {
                return 0;
            }
        }
        return 1;
    }
	
    int GetPrime(int n)
    {
        if (n % 2 == 0)
        {
            n++;
        }
        while (!CheckPrime(n))
        {
            n += 2;
        }
        return n;
    }

	// Хорошая хеш-функция основанная на деление?
    int hashFunction(int key)
    {
        return (key % Capacity);
    }
	
    void DisplayHash();
};

HashTable::HashTable(int c)
{
    int size = GetPrime(c);
    this->Capacity = size;
    Table = new list<int>[Capacity];
}

void HashTable::InsertItem(int key, int data)
{
	//Вычисляем хеш для таблицы с помощью хеш-функции
    int index = hashFunction(key);
    Table[index].push_back(data);
}

void HashTable::DeleteItem(int key)
{
    int index = hashFunction(key);

    list<int>::iterator i;
    for (i = Table[index].begin();
        i != Table[index].end(); i++)
    {
        if (*i == key)
            break;
    }

    if (i != Table[index].end())
        Table[index].erase(i);
}

void HashTable::DisplayHash()
{
    for (int i = 0; i < Capacity; i++)
    {
        cout << "Table[" << i << "]";
    	
        for (auto x : Table[i])
        {
	        cout << " --> " << x;
        }
    	
        cout << endl;
    }
}

int main()
{
    int key[] = { 231, 321, 212, 321, 433, 262 };

    int data[] = { 123, 432, 523, 43, 423, 111 };

    int size = sizeof(key) / sizeof(key[0]);

	//Создание хеш-таблицы с фиксированным размером?
    HashTable h(size);

    for (int i = 0; i < 5; i++)
    {
	    h.InsertItem(key[i], data[i]);
    }

    h.DeleteItem(12);
    h.DisplayHash();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
